export const isString = (value) => typeof value === 'string';
export const isNumber = (value) => typeof value === 'number';
export const isArray = (value) => Array.isArray(value);
export const isNull = (value) => value === null;
export const isObject = (value) => typeof value === 'object' && !isNull(value) && !isArray(value);
export const isBoolean = (value) => typeof value === 'boolean';
