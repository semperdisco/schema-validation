import { validate } from '../../src/validation/schema-validation.mjs';
import { assert } from 'chai';

describe('Schema Validation', () => {
    it('Should throw an error when provided schema is invalid', () => {
        const schema = {
            firstName: 'string',
            lastName: false,
        };

        const object = {}

        assert.throws(
            () => validate(schema, object),
            Error,
            'Invalid argument for schema. Please provide an object which only contains values of type string.',
        );
    });

    it('Should throw an error when provided object to validate is invalid', () => {
        const schema = {
            firstName: 'string',
            lastName: 'string',
        };

        const object = null;

        assert.throws(
            () => validate(schema, object),
            Error,
            'Invalid argument provided for object. Please provide a value with type of object.',
        );
    });

    it('Should return an error when property is missing', () => {
        const schema = {
            firstName: 'string',
            lastName: 'string',
        };

        const object = {
            firstName: 'Henk',
        }

        const errors = validate(schema, object);
        const expectedErrors = [
            'Object is missing property with name lastName',
        ];

        assert.deepEqual(errors, expectedErrors);
    });

    it('Should return an error when property doesn\'t have the correct type', () => {
        const schema = {
            firstName: 'string',
            lastName: 'string',
        };

        const object = {
            firstName: 'Henk',
            lastName: true,
        }

        const errors = validate(schema, object);
        const expectedErrors = [
            'Invalid type provided for property with name lastName, expected string',
        ];

        assert.deepEqual(errors, expectedErrors);
    });

    it('Should return an empty array when object matches schema', () => {
        const schema = {
            firstName: 'string',
            lastName: 'string',
        };

        const object = {
            firstName: 'Henk',
            lastName: 'de Vries',
        }

        const errors = validate(schema, object);
        const expectedErrors = [];

        assert.deepEqual(errors, expectedErrors);
    });
});