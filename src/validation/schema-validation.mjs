import { isString, isNumber, isArray, isObject, isBoolean } from "./type-validation.mjs";

const validateType = (type) => {
    switch (type) {
        case 'string':
            return isString;
        case 'number':
            return isNumber;
        case 'array':
            return isArray;
        case 'object':
            return isObject;
        case 'boolean':
            return isBoolean;
    }
};

const isValidSchema = (schema) => isObject(schema) && Object.values(schema).every(isString);
const hasProperty = (object, property) => property in object;
const hasType = (property, type) => validateType(type)(property);

export const validate = (schema, object) => {
    if (!isValidSchema(schema)) {
        throw new Error('Invalid argument for schema. Please provide an object which only contains values of type string.');
    }

    if (!isObject(object)) {
        throw new Error('Invalid argument provided for object. Please provide a value with type of object.');
    }

    const validationRules = Object.entries(schema);
    const errors = [];

    validationRules.forEach(
        (validationRule) => {
            const [ property, type ] = validationRule;

            if (!hasProperty(object, property)) {
                errors.push(
                    `Object is missing property with name ${property}`,
                );

                // If the property is missing validating the type is unnecessary
                return;
            }

            if (!hasType(object[property], type)) {
                errors.push(
                    `Invalid type provided for property with name ${property}, expected ${type}`,
                );
            }
        }
    );

    return errors;
};

export const composeValidateFunction = schema => object => validate(schema, object);