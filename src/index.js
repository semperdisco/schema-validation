import { barSchema, carSchema, personSchema } from './schemas.mjs';
import { bars, cars, persons } from "./data.mjs";
import { composeValidateFunction } from "./validation/schema-validation.mjs";

const validateBar = composeValidateFunction(barSchema);
const validateCar = composeValidateFunction(carSchema);
const validatePerson = composeValidateFunction(personSchema);

const hasErrors = (errors) => errors.length > 0;

bars.forEach(
    bar => {
        const validationResults = validateBar(bar);

        if (hasErrors(validationResults)) {
            console.log(`Bar with name "${bar.name}" is invalid and has the following errors:`, validationResults)
        } else {
            console.log(`Bar with name "${bar.name}" is valid.`);
        }
    },
);

cars.forEach(
    car => {
        const validationResults = validateCar(car);

        if (hasErrors(validationResults)) {
            console.log(`Car with brand "${car.brand}" is invalid and has the following errors:`, validationResults)
        } else {
            console.log(`Car with brand "${car.brand}" is valid.`);
        }
    },
);

persons.forEach(
    person => {
        const validationResults = validatePerson(person);

        if (hasErrors(validationResults)) {
            console.log(`Person with name "${person.name}" is invalid and has the following errors:`, validationResults)
        } else {
            console.log(`Person with name "${person.name}" is valid.`);
        }
    },
);