import { isString, isNumber, isArray, isObject, isBoolean } from '../../src/validation/type-validation.mjs';
import { assert } from 'chai';

describe('Type Validation', () => {
    describe('isString', () => {
        it('Should return true when value is a string', () => {
            assert.isTrue(
                isString('Lorem Ipsum'),
            );
        });

        it('Should return false when value is not a string', () => {
            const faultyValues = [
                10,
                false,
                [],
                {},
                null,
            ];

            faultyValues.forEach(
                (faultyValue) => assert.isFalse(isString(faultyValue)),
            );
        });
    });

    describe('isNumber', () => {
        it('Should return true when value is a number', () => {
            assert.isTrue(isNumber(10));
        });

        it('Should return false when value is not a number', () => {
            const faultyValues = [
                '10',
                false,
                [],
                {},
                null,
            ];

            faultyValues.forEach(
                (faultyValue) => assert.isFalse(isNumber(faultyValue)),
            );
        });
    });

    describe('isArray', () => {
        it('Should return true when value is an array', () => {
            assert.isTrue(isArray([]));
        });

        it('Should return false when value is not an array', () => {
            const faultyValues = [
                10,
                false,
                '[]',
                {},
                null,
            ];

            faultyValues.forEach(
                (faultyValue) => assert.isFalse(isArray(faultyValue)),
            );
        });
    });

    describe('isObject', () => {
        it('Should return true when value is an object', () => {
            assert.isTrue(isObject({}));
        });

        it('Should return false when value is not an object', () => {
            const faultyValues = [
                10,
                false,
                [],
                '{}',
                null,
            ];

            faultyValues.forEach(
                (faultyValue) => assert.isFalse(isObject(faultyValue)),
            );
        });
    });

    describe('isBoolean', () => {
        it('Should return true when value is a boolean', () => {
            assert.isTrue(isBoolean(true));
            assert.isTrue(isBoolean(false));
        });

        it('Should return false when value is not a boolean', () => {
            const faultyValues = [
                10,
                'false',
                [],
                {},
                null,
            ];

            faultyValues.forEach(
                (faultyValue) => assert.isFalse(isBoolean(faultyValue)),
            );
        });
    });
});