export const bars = [
    {
        name: 'Jimmys drinks',
        address: 'Somewhere over the rainbow',
        drinks: {
            beer: ['Straffe Hendrik', 'Rochefort', 'St Bernard'],
        },
    },
    {
        name: 'Sjonnies',
        address: 'Centrum 001',
        drinks: [ // < No object
            'Heineken',
        ]
    },
];

export const cars = [
    {
        brand: 'Mazda',
        type: 'MX5 NB 1.8',
        milage: 199999.99,
        extras: [
            '2001 Special Edition'
        ],
    },
    {
        brand: 'BMW',
        type: '335',
        milage: '100000', // < No number
        extras: [
            'LCI',
            'KW Coilovers',
        ],
    },
];

export const persons = [
    {
        name: 'James',
        age: 25,
        siblings: [
            'Johnathan',
        ],
        metaData: {},
        active: true,
    },
    {
        name: 'James',
        age: 25,
        siblings: [
            'Johnathan',
        ],
    },
];