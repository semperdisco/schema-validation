# Schema validation

A simple function to validate an object against a schema.

## Usage

The function expects a schema and an object to validate. The object that undergoes the validation is called target object from this point on.

The schema should contain the properties that are required on the target object. 
All properties defined in the schema are required, it is not possible to define an optional property. 
The value of every property in the schema represent the type that the property on the target object should have.

The types are represented by a string in full lowercase.

The supported types are:
- string
- number
- array
- object
- boolean

``` javascript
const validSchema = {
    name: 'string',
    age: 'number',
    active: 'boolean',
};

# Invalid because 'str' does not belong to the supported types
const invalidSchema = {
    name: 'str',
};

# Invalid because the value is not a string
const invalidSchema = {
    name: 12,
};
```

```` javascript
 validate(
    validSchema,
    {
        name: 'Henk de Vries',
        age: 20,
        active: true,
    },
 );
````

To make composition easier and to avoid repetition of calling the validate function with the same schema.
The module exports a function called `composeValidateFunction`.

By using partial application the function return a new function which already contains the schema to validate.
So for successive calls you only need to provide the object.

``` javascript
const schema = {
    name: 'string',
};


const validateObj = composeValidateFunction(schema);

validateObj({
    name: 'Bob',
});

validateObj({
    name: 'Piet',
});
```

## Getting started
``` bash
# Clone the repo
git clone git@gitlab.com:semperdisco/schema-validation.git
 
# Change the directory
cd schema-validation

# Install the dependencies
npm install
````

## Run the examples
The project contains a script with examples. To run the examples execute the following command:

``` bash
npm start
```

## Test
``` bash
npm test
```